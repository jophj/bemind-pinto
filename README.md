# Bemind - Pinto

----
## Installation
Clone the repository with
```git clone git@gitlab.com:jophj/bemind-pinto.git```.

Install dependencies:
`npm install`

## Configuration
The params needed are a MongoDB instance connection string and the server listening port.

You can rename the `config.example.json` to `config.json` and edit the `connectionString` and `port` properties or use the environment variables `CONNECTION_STRING` and `PORT`.

File configuration overrides environment configuration.

## Start
`npm start`
