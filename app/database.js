var mongoose = require('mongoose')

module.exports = {
  connect: function (connectionString) {
    return new Promise((resolve, reject) => {
      mongoose.connect(connectionString)
      const db = mongoose.connection
      db.on('error', function (err) {
        reject(err)
      })

      db.once('open', function () {
        resolve(db)
      })
    })
  }
}
