const request = require('supertest')
const app = require('./app.js')

describe('POST /orders', function () {
  it('should respond 405', function (done) {
    request(app)
      .post('/orders')
      .expect(405, done)
  })
})
