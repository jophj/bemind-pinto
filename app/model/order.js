const mongoose = require('mongoose')
const itemSchema = require('./item').schema

const orderSchema = mongoose.Schema({
  id: String,
  externalId: {type: String, index: true},
  channel: String,
  purchaseDate: Date,
  lastUpdate: Date,
  customerEmail: String,
  items: [itemSchema]
})

const Order = mongoose.model('Order', orderSchema)

module.exports = {
  schema: orderSchema,
  model: Order
}
