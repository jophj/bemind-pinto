const mongoose = require('mongoose')

const itemSchema = mongoose.Schema({
  sku: {type: String},
  name: String,
  qt: Number,
  price: Number
})

const Item = mongoose.model('Item', itemSchema)

module.exports = {
  schema: itemSchema,
  model: Item
}
