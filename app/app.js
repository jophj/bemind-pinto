const express = require('express')
const ordersRouter = require('./components/orders')
const app = express()

app.use(express.json())
app.use('/orders', ordersRouter)

module.exports = app
