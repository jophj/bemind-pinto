const assert = require('assert')
const mockery = require('mockery')
const sinon = require('sinon')

describe('Orders POST controller', function () {
  let PostController = null
  const addToDbMock = sinon.stub()
  const updateToDbMock = sinon.stub()
  const getOrderByExternalIdMock = sinon.stub()

  before(function () {
    mockery.enable()
    mockery.registerMock('./repositories/create', addToDbMock)
    mockery.registerMock('./repositories/update', updateToDbMock)
    mockery.registerMock('./repositories/getByExternalId', getOrderByExternalIdMock)
    PostController = require('./postController')
  })

  after(function () {
    mockery.disable()
  })

  afterEach(function () {
    addToDbMock.reset()
  })

  describe('post function', function () {
    it('should respond 500 if mapping fails', async function () {
      const controller = new PostController(e => null)
      const resMock = { sendStatus: sinon.fake() }

      await controller.post({ body: {} }, resMock)

      assert(resMock.sendStatus.calledWith(500))
    })

    describe('with a new order', function () {
      it('should store the order', async function () {
        const newOrder = {
          externalId: 'new id'
        }
        const resFake = { send: sinon.fake() }
        const controller = new PostController(e => e)

        await controller.post({body: [newOrder]}, resFake)

        assert(addToDbMock.calledWith(newOrder))
      })
    })

    describe('with an existing order', function () {
      it('should not update the order', async function () {
        const oldOrder = {
          externalId: 'existingId',
          lastUpdate: new Date()
        }
        getOrderByExternalIdMock.returns(oldOrder)
        const resFake = { send: sinon.fake() }
        const controller = new PostController(e => e)

        await controller.post({body: [oldOrder]}, resFake)

        assert(addToDbMock.notCalled)
        assert(updateToDbMock.notCalled)
      })
    })

    describe('with an updated order', function () {
      it('should update the order', async function () {
        const oldOrder = {
          _id: '_id',
          externalId: 'existingId',
          lastUpdate: new Date()
        }
        const updatedOrder = {
          externalId: 'existingId',
          lastUpdate: new Date(oldOrder.lastUpdate.getTime() + 1)
        }
        getOrderByExternalIdMock.returns(oldOrder)
        const resFake = { send: sinon.fake() }
        const controller = new PostController(e => e)

        await controller.post({body: [updatedOrder]}, resFake)

        assert(addToDbMock.notCalled)
        assert(updateToDbMock.calledWith(oldOrder._id, updatedOrder))
      })
    })
  })
})
