const add = require('./repositories/create')
const getByExternalId = require('./repositories/getByExternalId')
const update = require('./repositories/update')

async function updateOrCreate (order) {
  const dbOrder = await getByExternalId(order.externalId, order.channel)

  if (!dbOrder) {
    await add(order)
  } else if (order.lastUpdate > dbOrder.lastUpdate) {
    await update(dbOrder._id, order)
  }
}

class PostController {
  constructor (orderMapper) {
    this.orderMapper = orderMapper
  }

  async post (request, response) {
    if (!this.orderMapper) {
      throw new Error('No order mapper found')
    }
    const orders = this.orderMapper(request.body)
    if (!orders) {
      return response.sendStatus(500)
    }

    await Promise.all(orders.map(updateOrCreate))
    response.send(orders)
  }
}

module.exports = PostController
