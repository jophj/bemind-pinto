const assert = require('assert')
const mockery = require('mockery')
const sinon = require('sinon')

describe('Orders GET controller', function () {
  let controller = null
  const getPaginatedStub = sinon.spy()

  before(function () {
    mockery.enable()
    mockery.registerMock('./repositories/getPaginated', getPaginatedStub)
    controller = require('./getController')
  })

  describe('with no optional parameter', function () {
    it('should get all orders', async function () {
      getPaginatedStub.resetHistory()
      const resMock = { send: sinon.fake() }

      await controller({ query: {} }, resMock)

      assert(getPaginatedStub.calledOnceWith(NaN, NaN, undefined))
    })
  })

  describe('with offset', function () {
    it('should skip [offset] orders', async function () {
      getPaginatedStub.resetHistory()
      const expectedOffset = 1
      const resMock = { send: sinon.fake() }

      await controller({ query: { offset: expectedOffset } }, resMock)

      assert(getPaginatedStub.calledOnceWith(expectedOffset, NaN, undefined))
    })
  })

  describe('with limit', function () {
    it('should get at most [limit] orders', async function () {
      getPaginatedStub.resetHistory()
      const expectedLimit = 10
      const resMock = { send: sinon.fake() }

      await controller({ query: { limit: expectedLimit } }, resMock)

      assert(getPaginatedStub.calledOnceWith(NaN, expectedLimit, undefined))
    })
  })

  describe('with channel', function () {
    it('should get all [channel] orders', async function () {
      getPaginatedStub.resetHistory()
      const expectedChannel = 'eby'
      const resMock = { send: sinon.fake() }

      await controller({ query: { channel: expectedChannel } }, resMock)

      assert(getPaginatedStub.calledOnceWith(NaN, NaN, expectedChannel))
    })
  })
})
