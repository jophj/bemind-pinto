function itemMapper (item) {
  if (!item) {
    return null
  }
  return {
    name: item.title && item.title[0],
    sku: item.sku && item.sku[0],
    qt: item.qt && parseFloat(item.qt[0]),
    price: item.price && parseFloat(item.price[0])
  }
}

function orderMapper (amzOrder) {
  const items = amzOrder.items && amzOrder.items[0] && amzOrder.items[0].item
  return {
    channel: 'amz',
    externalId: amzOrder.id && amzOrder.id[0],
    purchaseDate: new Date(amzOrder.purchasedate),
    lastUpdate: new Date(amzOrder.lastupdatedate),
    customerEmail: amzOrder.customer && amzOrder.customer.email,
    items: Array.isArray(items) ? items.map(itemMapper) : [itemMapper(items)]
  }
}

module.exports = function (amzOrders) {
  if (Array.isArray(amzOrders.orders.order)) {
    return amzOrders.orders.order.map(orderMapper)
  } else {
    return [orderMapper(amzOrders.orders.order)]
  }
}
