const assert = require('assert')
const amzOrderMapper = require('./amzOrderMapper')

describe('Amz order mapper', function () {
  describe('with a valid order', function () {
    it('should map single Order to []', function () {
      const orders = { orders: { order: {} } }

      const mappedOrder = amzOrderMapper(orders)

      assert(Array.isArray(mappedOrder))
    })

    it('should map multiple orders to []', function () {
      const orders = { order: [{}, {}] }

      const mappedOrder = amzOrderMapper({orders})

      assert(Array.isArray(mappedOrder))
    })

    it('should map Id to externalId', function () {
      const orders = { order: {
        id: ['OrderId']
      }}

      const mappedOrder = amzOrderMapper({ orders })

      assert.strictEqual(orders.order.id[0], mappedOrder[0].externalId)
    })

    it('should set channel to amz', function () {
      const orders = { order: {
        id: 'OrderId'
      }}

      const mappedOrder = amzOrderMapper({ orders })

      assert.strictEqual('amz', mappedOrder[0].channel)
    })

    it('should map PurchaseDate to purchaseDate', function () {
      const purchaseDateValue = '2017-01-20T19:49:35Z'
      const orders = {
        order: { purchasedate: purchaseDateValue }
      }

      const mappedOrder = amzOrderMapper({ orders })

      assert.strictEqual(1484941775000, mappedOrder[0].purchaseDate.getTime())
    })

    it('should map LastUpdate to lastUpdate', function () {
      const lastUpdateValue = '2017-01-20T19:49:35Z'
      const orders = {
        order: { lastupdatedate: lastUpdateValue }
      }

      const mappedOrder = amzOrderMapper({ orders })

      assert.strictEqual(1484941775000, mappedOrder[0].lastUpdate.getTime())
    })

    it('should map Customer.Email to customerEmail', function () {
      const expectedCustomerEmail = 'test@mail.com'
      const orders = {
        order: { customer: { email: expectedCustomerEmail } }
      }

      const mappedOrder = amzOrderMapper({ orders })

      assert.strictEqual(expectedCustomerEmail, mappedOrder[0].customerEmail)
    })

    it('should map single Items.Item to []', function () {
      const orders = {
        order: { items: [{ item: {} }] }
      }

      const mappedOrder = amzOrderMapper({ orders })

      assert(Array.isArray(mappedOrder[0].items))
    })

    it('should map multiple Items.Item to []', function () {
      const orders = {
        order: { items: [{ item: [{}, {}] }] }
      }

      const mappedOrder = amzOrderMapper({ orders })

      assert(Array.isArray(mappedOrder[0].items))
      assert.strictEqual(orders.order.items[0].item.length, mappedOrder[0].items.length)
    })

    it('should map Items.Item properties', function () {
      const item = {
        sku: ['sku'],
        title: ['title'],
        qt: ['1'],
        price: ['1.99']
      }
      const expectedItem = {
        sku: 'sku',
        name: 'title',
        qt: 1,
        price: 1.99
      }
      const orders = {
        order: { items: [{ item }] }
      }

      const mappedOrder = amzOrderMapper({ orders })

      assert.deepStrictEqual(expectedItem, mappedOrder[0].items[0])
    })
  })
})
