const getPaginated = require('./repositories/getPaginated')

module.exports = async function (request, response) {
  const params = request.query
  const orders = await getPaginated(parseInt(params.offset), parseInt(params.limit), params.channel)
  response.send(orders)
}
