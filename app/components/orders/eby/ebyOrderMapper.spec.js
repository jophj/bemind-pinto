const assert = require('assert')
const ebyOrderMapper = require('./ebyOrderMapper')

describe('Eby order mapper', function () {
  describe('with a valid order', function () {
    it('should map orders to []', function () {
      const orders = []

      const mappedOrder = ebyOrderMapper(orders)

      assert(Array.isArray(mappedOrder))
    })

    it('should map id to externalId', function () {
      const orders = [{ id: 'ebyId' }]

      const mappedOrder = ebyOrderMapper(orders)

      assert.strictEqual(orders[0].id, mappedOrder[0].externalId)
    })

    it('should set channel to eby', function () {
      const orders = [{ id: 'ebyId' }]

      const mappedOrder = ebyOrderMapper(orders)

      assert.strictEqual('eby', mappedOrder[0].channel)
    })

    it('should map purchased_on to purchaseDate', function () {
      const purchaseDateValue = '2017-01-20T19:49:35Z'
      const orders = [{ purchased_on: purchaseDateValue }]

      const mappedOrder = ebyOrderMapper(orders)

      assert.strictEqual(1484941775000, mappedOrder[0].purchaseDate.getTime())
    })

    it('should map last_update to purchaseDate', function () {
      const lastUpdateValue = '2017-01-20T19:49:35Z'
      const orders = [{ last_update: lastUpdateValue }]

      const mappedOrder = ebyOrderMapper(orders)

      assert.strictEqual(1484941775000, mappedOrder[0].lastUpdate.getTime())
    })

    it('should map customer_email to customerEmail', function () {
      const expectedCustomerEmail = 'test@mail.com'
      const orders = [{ customer_email: expectedCustomerEmail }]

      const mappedOrder = ebyOrderMapper(orders)

      assert.strictEqual(expectedCustomerEmail, mappedOrder[0].customerEmail)
    })

    it('should map products to []', function () {
      const orders = [{ products: [{}, {}] }]

      const mappedOrder = ebyOrderMapper(orders)

      assert(Array.isArray(mappedOrder[0].items))
      assert.strictEqual(orders[0].products.length, mappedOrder[0].items.length)
    })

    it('should map products properties', function () {
      const expectedProduct = {
        sku: 'sku',
        title: 'title',
        qt: 1,
        price: 1.99
      }
      const orders = [{ products: [expectedProduct] }]

      const mappedOrder = ebyOrderMapper(orders)

      assert.deepStrictEqual(expectedProduct, mappedOrder[0].items[0])
    })
  })
})
