function orderMapper (ebyOrder) {
  return {
    channel: 'eby',
    externalId: ebyOrder.id,
    purchaseDate: new Date(ebyOrder.purchased_on),
    lastUpdate: new Date(ebyOrder.last_update),
    customerEmail: ebyOrder.customer_email,
    items: ebyOrder.products
  }
}

module.exports = function (ebyOrders) {
  return ebyOrders.map(orderMapper)
}
