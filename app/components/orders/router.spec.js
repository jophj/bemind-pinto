const assert = require('assert')
const request = require('supertest')
const express = require('express')
const router = require('./router.js')

const app = express()
app.use(express.json())
app.use(router)

const amzValidPayload =
`<Orders>
  <Order>
      <Id>902-3159896-1390916</Id>
      <PurchaseDate>2017-01-25T19:49:35Z</PurchaseDate>
      <LastUpdateDate>2017-01-25T19:49:35Z</LastUpdateDate>
      <Customer>
          <Email>customer1@example.com</Email>
      </Customer>
      <Items>
          <Item>
              <SKU>my-product-sku</SKU>
              <Title>Example item name</Title>
              <Qt>1</Qt>
              <Price>9.99</Price>
          </Item>
      </Items>
  </Order>
</Orders>
`
const amzInvalidPayload = '<Title>Example item name</Name>'
const ebyValidPayload =
`[
  {
      "id": "902-3159896-1390916",
      "purchased_on": "2017-01-25T19:49:35Z",
      "last_update": "2017-01-25T19:49:35Z",
      "customer_email": "customer2@example.comasdasd",
      "products": [
          {"sku": "my-product-sku", "name": "Example item name", "qt": 1, "price": 9.99}
      ]
  }
]`
const ebyInvalidPayload = `{ id: 'internal-id' }`

describe('POST /amz', function () {
  describe('with valid request', function () {
    it('should respond 200', function (done) {
      request(app)
        .post('/amz')
        .send(amzValidPayload)
        .set('Content-Type', 'application/xml')
        .expect(200)
        .end(done)
    })

    it('should respond with the stored or updated order', function (done) {
      request(app)
        .post('/amz')
        .send(amzValidPayload)
        .set('Content-Type', 'application/xml')
        .expect(response => {
          const order = response.body[0]
          assert.strictEqual('902-3159896-1390916', order.externalId)
          assert.strictEqual('amz', order.channel)
          assert.strictEqual('2017-01-25T19:49:35.000Z', order.purchaseDate)
          assert.strictEqual('2017-01-25T19:49:35.000Z', order.lastUpdate)
          assert.deepStrictEqual({ sku: 'my-product-sku', name: 'Example item name', qt: 1, price: 9.99 }, order.items[0])
        })
        .end(done)
    })
  })

  describe('with invalid request', function () {
    it('should respond 400', function (done) {
      request(app)
        .post('/amz')
        .send(amzInvalidPayload)
        .set('Content-Type', 'application/xml')
        .expect(400, done)
    })
  })
})

describe('POST /eby', function () {
  describe('with valid request', function () {
    it('should respond 200', function (done) {
      request(app)
        .post('/eby')
        .send(ebyValidPayload)
        .set('Content-Type', 'application/json')
        .expect(200, done)
    })

    it('should respond with the stored or updated order', function (done) {
      request(app)
        .post('/eby')
        .send(ebyValidPayload)
        .set('Content-Type', 'application/json')
        .expect(response => {
          const order = response.body[0]
          assert.strictEqual('902-3159896-1390916', order.externalId)
          assert.strictEqual('eby', order.channel)
          assert.strictEqual('2017-01-25T19:49:35.000Z', order.purchaseDate)
          assert.strictEqual('2017-01-25T19:49:35.000Z', order.lastUpdate)
          assert.deepStrictEqual({ sku: 'my-product-sku', name: 'Example item name', qt: 1, price: 9.99 }, order.items[0])
        })
        .end(done)
    })
  })

  describe('with invalid request', function () {
    it('should respond 400', function (done) {
      request(app)
        .post('/eby')
        .send(ebyInvalidPayload)
        .set('Content-Type', 'application/json')
        .expect(400, done)
    })
  })
})
