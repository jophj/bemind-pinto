const Order = require('../../../model/order').model

module.exports = async function (order) {
  const toCreate = new Order(order)
  return toCreate.save()
}
