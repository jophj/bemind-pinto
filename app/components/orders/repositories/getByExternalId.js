const Order = require('../../../model/order').model

module.exports = async function (externalId, channel) {
  return Order
    .findOne({
      externalId,
      channel
    })
}
