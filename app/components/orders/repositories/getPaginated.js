const Order = require('../../../model/order').model

module.exports = async function (offset = 0, limit = 0, channel = null) {
  let query = Order
  if (channel) {
    query = query.find({ channel })
  } else {
    query = query.find()
  }
  if (offset) {
    query = query.skip(offset)
  }
  if (limit) {
    query = query.limit(limit)
  }

  return query.exec()
}
