const Order = require('../../../model/order').model

module.exports = async function (id, order) {
  return Order.update({ _id: id }, { $set: order })
}
