const express = require('express')
const xmlparser = require('express-xml-bodyparser')

const amzOrderMapper = require('./amz/amzOrderMapper')
const ebyOrderMapper = require('./eby/ebyOrderMapper')
const PostController = require('./postController.js')
const getController = require('./getController.js')

const amzController = new PostController(amzOrderMapper)
const ebyController = new PostController(ebyOrderMapper)
const router = express.Router()

router.get('/', getController)

router.post('/', function (req, res) {
  res.sendStatus(405)
})

router.post('/amz', xmlparser(), amzController.post.bind(amzController))
router.post('/eby', ebyController.post.bind(ebyController))

module.exports = router
