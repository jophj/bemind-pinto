const http = require('http')
const database = require('./app/database')
const app = require('./app/app.js')

let configuration = {}
try {
  configuration = require('./config.json')
} catch (e) {
  console.log('No configuration file found, using environment')
}
const port = configuration.port || process.env.PORT || '3000'
const connectionString = configuration.connectionString || process.env.CONNECTION_STRING
async function Main () {
  try {
    await database.connect(connectionString)
    console.log('Database connected')
  } catch (e) {
    console.error(e)
  }

  http.createServer(app).listen(port)
  console.log('Listening on port', port)
}

Main().catch(err =>
  console.error(err)
)
